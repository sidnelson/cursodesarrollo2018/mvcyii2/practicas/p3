DROP DATABASE IF EXISTS p3;
CREATE DATABASE p3;
USE p3;

DROP TABLE IF EXISTS notas;
CREATE TABLE notas (
  id_notas int AUTO_INCREMENT,
  fecha date,
  hora time,
  mensaje varchar(255),
  PRIMARY KEY (id_notas)
  );
